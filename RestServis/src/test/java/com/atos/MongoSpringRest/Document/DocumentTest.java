package com.atos.MongoSpringRest.Document;


import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Document.Document;
import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import com.atos.MongoSpringRest.Repositories.Document.DocumentRepository;
import com.atos.MongoSpringRest.Repositories.IspCaseSeqNum.IspCaseSeqNumRepository;
import com.atos.MongoSpringRest.Services.Document.DocumentService;
import com.atos.MongoSpringRest.Services.IspCaseSeqNum.IspCaseSeqNumService;
import com.atos.MongoSpringRest.Specifications.IspCaseSeqNum.IspCaseSeqNumSearchCriteria;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DocumentTest {

    private static final Long DOCUMENT_ID = Long.valueOf(236);
    private static final int DOCUMENT_TYPE_ID = 2;
    private static final String SENDER_TYPE_ID = "SEND_LEGAL";

    private static final String SENDER_NAME = "dfadsfds";

    private static final Date INITIAL_ACT_DATE_FROM = Date.valueOf("2021-09-01");
    private static final Date INITIAL_ACT_DATE_TO = Date.valueOf("2021-09-29");
    private static final int CASEFILE_ID =3206;

    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private DocumentService documentService;


    @Before
    public void setUp() throws Exception {

    }


    @Test
    @Order(1)
    public void testGetDocumentById() {
//given
        Document documentActual = new Document();
        documentActual.setCasefileId(CASEFILE_ID);
        documentActual.setId(DOCUMENT_ID);
        documentActual.setDocumentTypeId(DOCUMENT_TYPE_ID);
        documentActual.setSenderTypeId(SENDER_TYPE_ID);
        documentActual.setSenderName(SENDER_NAME);

        Optional<Document> expected =documentRepository.findById(DOCUMENT_ID);

        //then
        Assert.assertNotNull(expected);
        Document documentExpected = expected.get();

        Assert.assertEquals(documentExpected.getId(),documentActual.getId());
        Assert.assertEquals( documentExpected.getCasefileId(),documentActual.getCasefileId());
        Assert.assertEquals( documentExpected.getSenderName(), documentActual.getSenderName());
        Assert.assertEquals(documentExpected.getSenderTypeId(), documentActual.getSenderTypeId());
        Assert.assertEquals(documentExpected.getDocumentTypeId(), documentActual.getDocumentTypeId());
    }

}
