package com.atos.MongoSpringRest.IspCaseSeqNum;


import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Repositories.IspCaseSeqNum.IspCaseSeqNumRepository;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Services.IspCaseSeqNum.IspCaseSeqNumService;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.IspCaseSeqNum.IspCaseSeqNumSearchCriteria;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class IspCaseSeqNumTest {

    private static final Long ISPCASESEQNUM_ID = Long.valueOf(216);
    private static final int CLASS_ID = 67;
    private static final int CLASS_ID_DUMMY = 1000;

    private static final int CASE_YEAR = 2020;
    private static final String JUDICIAL_CODE = "ospg";
    private static final String JUDICIAL_CODE_DUMMY = "ospg90";


    @Autowired
    private IspCaseSeqNumRepository ispCaseSeqNumRepository;
    @Autowired
    private IspCaseSeqNumService ispCaseSeqNumService;


    @Before
    public void setUp() throws Exception {

    }


    @Test
    @Order(1)
    public void testGetIspCaseSeqNumById() {
        //given
        IspCaseSeqNum ispCaseSeqNumActual = new IspCaseSeqNum();
        ispCaseSeqNumActual.setId(ISPCASESEQNUM_ID);

        ispCaseSeqNumActual.setCaseClassId(CLASS_ID);
        ispCaseSeqNumActual.setCaseJudicialInstitutionCode(JUDICIAL_CODE);
        ispCaseSeqNumActual.setCaseYear(CASE_YEAR);

        Optional<IspCaseSeqNum> expected =ispCaseSeqNumRepository.findById(ISPCASESEQNUM_ID);

        //then
        Assert.assertNotNull(expected);
        IspCaseSeqNum ispCaseSeqNumExpected = expected.get();

        Assert.assertEquals(ispCaseSeqNumActual.getId(),ispCaseSeqNumExpected.getId());
        Assert.assertEquals( ispCaseSeqNumActual.getCaseJudicialInstitutionCode(),ispCaseSeqNumExpected.getCaseJudicialInstitutionCode());
        Assert.assertEquals( ispCaseSeqNumActual.getCaseClassId(), ispCaseSeqNumExpected.getCaseClassId());
        Assert.assertEquals(ispCaseSeqNumActual.getCaseYear(), ispCaseSeqNumExpected.getCaseYear());
    }
    @Test
    public void testGetCaseFileByCriteria() {
        IspCaseSeqNumSearchCriteria ispCaseSeqNumSearchCriteria = new IspCaseSeqNumSearchCriteria();
        ispCaseSeqNumSearchCriteria.setCaseYear(CASE_YEAR);
        ispCaseSeqNumSearchCriteria.setClassId(CLASS_ID);
        ispCaseSeqNumSearchCriteria.setJudicialCode(JUDICIAL_CODE);

        long result = ispCaseSeqNumService.findMaxIdBySearchCriteria(ispCaseSeqNumSearchCriteria);
        Assert.assertEquals(97,result);


    }

    @Test
    public void testGetCaseFileByCriteriaWithWrongYear() {
        IspCaseSeqNumSearchCriteria ispCaseSeqNumSearchCriteria = new IspCaseSeqNumSearchCriteria();
        ispCaseSeqNumSearchCriteria.setCaseYear(CASE_YEAR);
        ispCaseSeqNumSearchCriteria.setClassId(CLASS_ID);
        ispCaseSeqNumSearchCriteria.setJudicialCode(JUDICIAL_CODE_DUMMY);

        long result = ispCaseSeqNumService.findMaxIdBySearchCriteria(ispCaseSeqNumSearchCriteria);
        Assert.assertEquals(1,result);
    }

    @Test
    public void testGetCaseFileByCriteriaWithWrongClassId() {
        IspCaseSeqNumSearchCriteria ispCaseSeqNumSearchCriteria = new IspCaseSeqNumSearchCriteria();
        ispCaseSeqNumSearchCriteria.setCaseYear(CASE_YEAR);
        ispCaseSeqNumSearchCriteria.setClassId(CLASS_ID_DUMMY);
        ispCaseSeqNumSearchCriteria.setJudicialCode(JUDICIAL_CODE);

        long result = ispCaseSeqNumService.findMaxIdBySearchCriteria(ispCaseSeqNumSearchCriteria);
        Assert.assertEquals(1,result);
    }
}
