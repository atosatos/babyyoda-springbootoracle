package com.atos.MongoSpringRest.CasefileTest;


import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CasefileCustomDialect {

    private static final Long CASE_FILE_ID = Long.valueOf(3823);
    private static final int CLASS_ID = 67;
    private static final int CASE_YEAR = 2020;
    private static final int CASE_SN = 42;
    private static final int JUDGE_ID = 74;
    private static final Date INITIAL_ACT_DATE_FROM = Date.valueOf("2021-01-01");
    private static final Date INITIAL_ACT_DATE_TO = Date.valueOf("2021-01-31");

    @Autowired
    private CasefileRepository casefileRepository;
    @Autowired
    private CasefileService casefileService;


    @Before
    public void setUp() throws Exception {

    }


    @Test
    @Order(1)
    public void testGetCaseFileById() {
        //given
        Casefile caseFileActual = new Casefile();
        caseFileActual.setId(CASE_FILE_ID);
        caseFileActual.setJudgeId(JUDGE_ID);
        caseFileActual.setClassId(CLASS_ID);
        caseFileActual.setCaseSn(CASE_SN);
        caseFileActual.setCaseYear(CASE_YEAR);



        Optional<Casefile> expected =casefileRepository.findById(CASE_FILE_ID);

        //then
        Assert.assertNotNull(expected);
        Casefile casefileExpected = expected.get();

        Assert.assertEquals((Long) caseFileActual.getId(),CASE_FILE_ID);
        Assert.assertEquals( caseFileActual.getJudgeId(),JUDGE_ID);
        Assert.assertEquals( caseFileActual.getClassId(), Optional.of(CLASS_ID).get());
        Assert.assertEquals(caseFileActual.getCaseSn(), Optional.of(CASE_SN).get());
        Assert.assertEquals(caseFileActual.getCaseYear(), Optional.of(CASE_YEAR).get());
    }
    @Test
    public void testGetCaseFileByCriteria() {
        Casefile caseFileActual = new Casefile();
        caseFileActual.setId(CASE_FILE_ID);
        caseFileActual.setJudgeId(JUDGE_ID);
        caseFileActual.setClassId(CLASS_ID);
        caseFileActual.setCaseSn(CASE_SN);
        caseFileActual.setCaseYear(CASE_YEAR);

        List<Casefile> expected =casefileRepository.getCaseFileByCriteria(CLASS_ID,CASE_SN, Optional.of(CASE_YEAR));

        Assert.assertEquals(expected.get(0).getClassId(), Optional.of(CLASS_ID).get());
        Assert.assertEquals(expected.get(0).getCaseSn(), Optional.of(CASE_SN).get());
        Assert.assertEquals(expected.get(0).getCaseYear(), Optional.of(CASE_YEAR).get());
    }
    @Test
    public void testGetCaseFileBySearchCriteriaByClassIdAndCaseSn() {
        CasefileSearchCriteria searchCriteria = new CasefileSearchCriteria();
        searchCriteria.setClassId(CLASS_ID);
        searchCriteria.setCaseSn(CASE_SN);

        List<Casefile> listCasefiles =casefileService.findCaseFileBySearchCriteria(searchCriteria);

        Assert.assertEquals(2,listCasefiles.size());


    }

    @Test
    public void testGetCaseFileBySearchCriteriaGreaterThanDate() {
        CasefileSearchCriteria searchCriteria = new CasefileSearchCriteria();
        searchCriteria.setInitialDateFrom(INITIAL_ACT_DATE_FROM);

        List<Casefile> listCasefiles =casefileService.findCaseFileBySearchCriteria(searchCriteria);
        Assert.assertEquals(740,listCasefiles.size());

    }

    @Test
    public void testGetCaseFileBySearchCriteriaBetweenDate() {
        CasefileSearchCriteria searchCriteria = new CasefileSearchCriteria();
        searchCriteria.setInitialDateFrom(INITIAL_ACT_DATE_FROM);
        searchCriteria.setInitialDateTo(INITIAL_ACT_DATE_TO);

        List<Casefile> listCasefiles =casefileService.findCaseFileBySearchCriteria(searchCriteria);
        Assert.assertEquals(153,listCasefiles.size());

    }


}
