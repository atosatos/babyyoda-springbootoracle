package com.atos.MongoSpringRest.IspPickListTest;

import com.atos.MongoSpringRest.Models.IspPickList.IspPickList;
import com.atos.MongoSpringRest.Repositories.IspPickList.IspPickListRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class IspPickListServiceCustomDialect {


    private static final String ISP_PICK_LIST_ID = "Proba1";
    private static final String ISP_PICK_LIST_NAME = "Proba1";
    private static final String ISP_PICK_LIST_TYPE = "Proba1";

    @Autowired
    private IspPickListRepository ispPickListRepository;


    @Before
    public void setUp() throws Exception {

    }


    @Test
    @Order(1)
    public void testSaveOrUpdateIspPickList() {
        //given
        IspPickList ispPickListActual = new IspPickList();
        ispPickListActual.setId(ISP_PICK_LIST_ID);
        ispPickListActual.setPick_list_type(ISP_PICK_LIST_TYPE);
        ispPickListActual.setName(ISP_PICK_LIST_NAME);
        //when
        ispPickListRepository.save(ispPickListActual);

        Optional<IspPickList> expected =ispPickListRepository.findById(ispPickListActual.getId());

        //then
        Assert.assertNotNull(expected);
        IspPickList ispPickListExpected = expected.get();
        Assert.assertEquals(ispPickListExpected.getName(),ISP_PICK_LIST_NAME);
        Assert.assertEquals(ispPickListExpected.getId(),ISP_PICK_LIST_ID);
        Assert.assertEquals(ispPickListExpected.getPick_list_type(),ISP_PICK_LIST_TYPE);
    }

    @Test
    @Order(2)
    public void testGetOneIspPickListById() {
        //given

        IspPickList ispPickListActual = new IspPickList();
        ispPickListActual.setId("SRB");
        ispPickListActual.setPick_list_type("country");
        ispPickListActual.setName("Srbija");

        Optional<IspPickList> test = ispPickListRepository.findById(ispPickListActual.getId());
        IspPickList expected = null;
        if(test.isPresent()){
            expected = test.get();
        }

        Assert.assertEquals(expected.getId(),ispPickListActual.getId());
        Assert.assertEquals(expected.getPick_list_type(),ispPickListActual.getPick_list_type());
        Assert.assertEquals(expected.getName(),ispPickListActual.getName());

    }

    @Test
    @Order(3)
    public void testAllIspPickList() {
        //given
        int expected =144;
        List<IspPickList> actualList = (List<IspPickList>) ispPickListRepository.findAll();

        Assert.assertEquals(expected,actualList.size());
    }
    @Test
    @Order(4)
    public void testzdeleteIspPickList() {
        ispPickListRepository.deleteById(ISP_PICK_LIST_ID);
        Optional<IspPickList> test = ispPickListRepository.findById(ISP_PICK_LIST_ID);

       // Assert.assertEquals(false,test.isPresent());
        Assert.assertTrue(!test.isPresent());

    }


}
