package com.atos.MongoSpringRest.Repositories.CriminalAct;
import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CriminalActRepository extends JpaRepository<CriminalAct, Long>, JpaSpecificationExecutor<CriminalAct> {

}
