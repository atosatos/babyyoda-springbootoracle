package com.atos.MongoSpringRest.Repositories.Content;
import com.atos.MongoSpringRest.Models.Content.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentRepository extends JpaRepository<Content, Long>, JpaSpecificationExecutor<Content> {

}
