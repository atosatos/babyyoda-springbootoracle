package com.atos.MongoSpringRest.Repositories.Party;

import com.atos.MongoSpringRest.Models.Party.Party;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PartyRepository extends JpaRepository<Party, Long>, JpaSpecificationExecutor<Party> {
}
