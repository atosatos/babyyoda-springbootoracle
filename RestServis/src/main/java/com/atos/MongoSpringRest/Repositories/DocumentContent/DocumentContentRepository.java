package com.atos.MongoSpringRest.Repositories.DocumentContent;
import com.atos.MongoSpringRest.Models.DocumentContent.DocumentContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DocumentContentRepository extends JpaRepository<DocumentContent, Long>, JpaSpecificationExecutor<DocumentContent> {

}
