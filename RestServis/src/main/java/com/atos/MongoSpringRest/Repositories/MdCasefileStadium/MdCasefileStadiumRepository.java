package com.atos.MongoSpringRest.Repositories.MdCasefileStadium;

import com.atos.MongoSpringRest.Models.MdCasefileStadium.MdCasefileStadium;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MdCasefileStadiumRepository extends JpaRepository<MdCasefileStadium, String> {
}
