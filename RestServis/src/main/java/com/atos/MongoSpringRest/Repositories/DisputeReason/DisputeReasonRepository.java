package com.atos.MongoSpringRest.Repositories.DisputeReason;

import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DisputeReasonRepository extends JpaRepository<DisputeReason, Long>, JpaSpecificationExecutor<DisputeReason> {
}
