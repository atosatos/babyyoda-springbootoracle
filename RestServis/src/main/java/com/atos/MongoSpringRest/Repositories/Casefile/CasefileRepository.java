package com.atos.MongoSpringRest.Repositories.Casefile;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CasefileRepository extends JpaRepository<Casefile, Long>, JpaSpecificationExecutor<Casefile> {

    public List<Casefile> getCaseFileByCaseNum(String caseNum);

    @Query(value="SELECT * from CaseFile c where (c.class_id =:classId OR :classId is Null) AND (c.case_sn =:caseSn OR :caseSn is Null) AND (:caseYear is Null OR c.case_year=:caseYear)", nativeQuery = true)
    public List<Casefile> getCaseFileByCriteria(@Param("classId") int classId, @Param("caseSn") int caseSn, @Param("caseYear") Optional<Integer> caseYear);

}
