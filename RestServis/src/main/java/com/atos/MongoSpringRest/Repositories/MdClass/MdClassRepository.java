package com.atos.MongoSpringRest.Repositories.MdClass;

import com.atos.MongoSpringRest.Models.MdClass.MdClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MdClassRepository extends JpaRepository<MdClass, String> {
}
