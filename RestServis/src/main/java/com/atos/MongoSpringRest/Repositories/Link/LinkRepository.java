package com.atos.MongoSpringRest.Repositories.Link;
import com.atos.MongoSpringRest.Models.Link.Link;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface LinkRepository extends JpaRepository<Link, Long>, JpaSpecificationExecutor<Link> {

}
