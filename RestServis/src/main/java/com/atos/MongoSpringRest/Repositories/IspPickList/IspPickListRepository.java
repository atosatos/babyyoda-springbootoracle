package com.atos.MongoSpringRest.Repositories.IspPickList;

import com.atos.MongoSpringRest.Models.IspPickList.IspPickList;
import org.springframework.data.repository.CrudRepository;

public interface IspPickListRepository extends CrudRepository<IspPickList, String> {
}
