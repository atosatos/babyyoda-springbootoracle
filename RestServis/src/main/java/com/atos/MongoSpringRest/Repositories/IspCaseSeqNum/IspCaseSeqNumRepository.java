package com.atos.MongoSpringRest.Repositories.IspCaseSeqNum;

import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IspCaseSeqNumRepository extends JpaRepository<IspCaseSeqNum, Long>, JpaSpecificationExecutor<IspCaseSeqNum> {



}
