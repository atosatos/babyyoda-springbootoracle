package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Casefile")
public class CasefileController implements ControllerInterface<Casefile,CasefileSearchCriteria> {
    private CasefileService casefileService;

    @Autowired
    public void setCasefileService(CasefileService casefileService) {
        this.casefileService = casefileService;
    }

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll/{page}", produces = "application/json")
    public List<Casefile> getAll(@PathVariable Integer page){
        try {
            List<Casefile> list = casefileService.listAll(page);
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public Casefile getById(@PathVariable Long id){
        try {
            return casefileService.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getWithSearchCriteria", produces = "application/json")
    public List<Casefile> getWithSearchCriteria(CasefileSearchCriteria searchCriteria) {
        return casefileService.findCaseFileBySearchCriteria(searchCriteria);
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public Casefile save(@RequestBody Casefile casefile){
        try {
            return casefileService.saveOrUpdate(casefile);
        } catch (Exception e) {
            return null;
        }
    }
}
