package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.DocumentContent.DocumentContent;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Services.DocumentContent.DocumentContentService;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/DocumentContent")
public class DocumentContentController implements ControllerInterface<DocumentContent,Integer> {
    private DocumentContentService documentContentService;

    @Autowired
    public void setDocumentContentService(DocumentContentService documentContentService) {
        this.documentContentService = documentContentService;
    }


    @Override
    public List<DocumentContent> getAll(@PathVariable Integer page) {
        throw new NotImplementedException();
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<DocumentContent> getAll() {
        try {
            List<DocumentContent> list = documentContentService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public DocumentContent getById(@PathVariable Long id) {
        try {
            return documentContentService.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<DocumentContent> getWithSearchCriteria(Integer searchCriteria) {
        throw  new NotImplementedException();
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public DocumentContent save(DocumentContent body) {
        try {
          return  documentContentService.saveOrUpdate(body);
        } catch (Exception e) {
            return null;
        }
    }
}
