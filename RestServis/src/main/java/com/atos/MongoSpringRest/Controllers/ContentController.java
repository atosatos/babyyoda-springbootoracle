package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.Content.Content;
import com.atos.MongoSpringRest.Services.Content.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@RestController
@RequestMapping("/Content")
public class ContentController implements ControllerInterface<Content,Integer> {
    private ContentService contentService;

    @Autowired
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Override
    public List<Content> getAll(@PathVariable Integer page) {
        try {
            List<Content> list = contentService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<Content> getAll() {
        try {
            List<Content> list = contentService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public Content getById(@PathVariable Long id) {
        try {
            return contentService.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Content> getWithSearchCriteria(Integer searchCriteria) {
        throw new NotImplementedException();
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public Content save(Content body) {
        try {
            return contentService.saveOrUpdate(body);
        } catch (Exception e) {
            return null;
        }
    }
}
