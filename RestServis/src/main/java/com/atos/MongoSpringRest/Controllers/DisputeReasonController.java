package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.atos.MongoSpringRest.Services.DisputeReason.DisputeReasonService;
import com.atos.MongoSpringRest.Services.Party.PartyService;
import com.atos.MongoSpringRest.Specifications.DisputeReason.DisputeReasonSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Party.PartySearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/DisputeReason")
public class DisputeReasonController implements ControllerInterface<DisputeReason,DisputeReasonSearchCriteria> {
    private DisputeReasonService disputeReasonService;

    @Autowired
    public void setDisputeReasonService(DisputeReasonService disputeReasonService) {
        this.disputeReasonService = disputeReasonService;
    }

    @Override
    public List<DisputeReason> getAll(@PathVariable Integer page) {
        try {
            List<DisputeReason> list = new ArrayList<>();
            list = disputeReasonService.listAll();

            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<DisputeReason> getAll() {
        try {
            List<DisputeReason> list = disputeReasonService.listAll();

            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public DisputeReason getById(@PathVariable Long id) {
        try {
            DisputeReason element  = disputeReasonService.getById(id);
            return element;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getWithSearchCriteria", produces = "application/json")
    public List<DisputeReason> getWithSearchCriteria(DisputeReasonSearchCriteria searchCriteria) {
        List<DisputeReason> disputeReasons = disputeReasonService.findDisputeReasonBySearchCriteria(searchCriteria);
        return disputeReasons;
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public DisputeReason save(DisputeReason body) {
        try {
            DisputeReason element  = disputeReasonService.saveOrUpdate(body);
            return element;
        } catch (Exception e) {
            return null;
        }
    }
}
