package com.atos.MongoSpringRest.Controllers;

import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Services.MdCasefileStadium.MdCasefileStadiumService;
import com.atos.MongoSpringRest.Models.MdCasefileStadium.MdCasefileStadium;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/MdCasefileStadium")
public class MdCasefileStadiumController implements ControllerInterface<MdCasefileStadium,Integer> {

    private MdCasefileStadiumService mdCasefileStadiumService;

    @Autowired
    public void setMdCasefileStadiumService(MdCasefileStadiumService mdCasefileStadiumService) {
        this.mdCasefileStadiumService = mdCasefileStadiumService;
    }


    @Override
    public List<MdCasefileStadium> getAll(@PathVariable Integer page) {
        throw new NotImplementedException();
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<MdCasefileStadium> getAll() {
        try {
            List<MdCasefileStadium> list = mdCasefileStadiumService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MdCasefileStadium getById(Long id) {
        throw  new NotImplementedException();
    }

    @Override
    public List<MdCasefileStadium> getWithSearchCriteria(Integer searchCriteria) {
        throw  new NotImplementedException();
    }

    @Override
    public MdCasefileStadium save(MdCasefileStadium body) {
        throw  new NotImplementedException();
    }
}
