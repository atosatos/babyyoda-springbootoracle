package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Document.Document;
import com.atos.MongoSpringRest.Services.Document.DocumentService;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Document.DocumentSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Document")
public class DocumentController implements ControllerInterface<Document,DocumentSearchCriteria> {
    private DocumentService documentService;

    @Autowired
    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }


    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll/{page}", produces = "application/json")
    public List<Document> getAll(@PathVariable Integer page) {
        try {
            List<Document> list = new ArrayList<>();
            list = documentService.listAll(page);

            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public Document getById(@PathVariable Long id) {
        try {
            Document element  = documentService.getById(id);
            return element;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getWithSearchCriteria", produces = "application/json")
    public List<Document> getWithSearchCriteria(DocumentSearchCriteria searchCriteria) {
        List<Document> casefiles = documentService.findDocumentBySearchCriteria(searchCriteria);
        return casefiles;
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public Document save(Document body) {
        try {
            Document element  = documentService.saveOrUpdate(body);
            return element;
        } catch (Exception e) {
            return null;
        }
    }
}
