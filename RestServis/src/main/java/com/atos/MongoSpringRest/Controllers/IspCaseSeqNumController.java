package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Services.IspCaseSeqNum.IspCaseSeqNumService;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.IspCaseSeqNum.IspCaseSeqNumSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/IspCaseSeqNum")
public class IspCaseSeqNumController implements ControllerInterface<IspCaseSeqNum,IspCaseSeqNumSearchCriteria> {
    private IspCaseSeqNumService ispCaseSeqNumService;

    @Autowired
    public void setIspCaseSeqNumService(IspCaseSeqNumService ispCaseSeqNumService) {
        this.ispCaseSeqNumService = ispCaseSeqNumService;
    }

    @Override
    public List<IspCaseSeqNum> getAll(Integer page) {
        throw  new NotImplementedException();
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public IspCaseSeqNum getById(@PathVariable Long id) {
        try {
            IspCaseSeqNum element  = ispCaseSeqNumService.getById(id);
            return element;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getWithSearchCriteria", produces = "application/json")
    public List<IspCaseSeqNum> getWithSearchCriteria(IspCaseSeqNumSearchCriteria searchCriteria) {
        throw  new NotImplementedException();
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public IspCaseSeqNum save(IspCaseSeqNum body) {
        try {
            IspCaseSeqNum element  = ispCaseSeqNumService.saveOrUpdate(body);
            return element;
        } catch (Exception e) {
            return null;
        }
    }


    public Long getMaxIdWithSearchCriteria(IspCaseSeqNumSearchCriteria searchCriteria) {
        long maxId = ispCaseSeqNumService.findMaxIdBySearchCriteria(searchCriteria);
        return maxId;
    }

}
