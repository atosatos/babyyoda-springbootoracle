package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.atos.MongoSpringRest.Services.CriminalAct.CriminalActService;
import com.atos.MongoSpringRest.Services.Party.PartyService;
import com.atos.MongoSpringRest.Specifications.CriminalAct.CriminalActSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Party.PartySearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/CriminalAct")
public class CriminalActController implements ControllerInterface<CriminalAct,CriminalActSearchCriteria> {
    private CriminalActService criminalActService;

    @Autowired
    public void setCriminalActService(CriminalActService criminalActService) {
        this.criminalActService = criminalActService;
    }


    @Override
    public List<CriminalAct> getAll(@PathVariable Integer page) {
        try {
            List<CriminalAct> list = new ArrayList<>();
            list = criminalActService.listAll();

            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<CriminalAct> getAll() {
        try {
            List<CriminalAct> list = criminalActService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public CriminalAct getById(@PathVariable Long id) {
        try {
            return criminalActService.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getWithSearchCriteria", produces = "application/json")
    public List<CriminalAct> getWithSearchCriteria(CriminalActSearchCriteria searchCriteria) {
        return criminalActService.findCriminalActBySearchCriteria(searchCriteria);
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public CriminalAct save(CriminalAct body) {
        try {
            return criminalActService.saveOrUpdate(body);
        } catch (Exception e) {
            return null;
        }
    }
}
