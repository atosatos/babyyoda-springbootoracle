package com.atos.MongoSpringRest.Controllers;


import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.atos.MongoSpringRest.Services.Link.LinkService;
import com.atos.MongoSpringRest.Services.Party.PartyService;
import com.atos.MongoSpringRest.Specifications.Link.LinkSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Party.PartySearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Party")
public class PartyController implements ControllerInterface<Party, PartySearchCriteria> {
    private PartyService partyService;

    @Autowired
    public void setPartyService(PartyService partyService) {
        this.partyService = partyService;
    }

    @Override
    public List<Party> getAll(@PathVariable Integer page) {
        throw new NotImplementedException();
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<Party> getAll() {
        try {
            List<Party> list = partyService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public Party getById(@PathVariable Long id) {
        try {
            return partyService.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getWithSearchCriteria", produces = "application/json")
    public List<Party> getWithSearchCriteria(PartySearchCriteria searchCriteria) {
        return partyService.findPartyBySearchCriteria(searchCriteria);
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/save", produces = "application/json")
    public Party save(Party body) {
        try {
            return partyService.saveOrUpdate(body);
        } catch (Exception e) {
            return null;
        }
    }
}
