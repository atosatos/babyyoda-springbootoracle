package com.atos.MongoSpringRest.Controllers;

import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.MdClass.MdClass;
import com.atos.MongoSpringRest.Services.MdClass.MdClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/MdClass")
public class MdClassController implements ControllerInterface<MdClass, Integer> {

    private MdClassService mdClassService;

    @Autowired
    public void setMdClassService(MdClassService mdClassService) {
        this.mdClassService = mdClassService;
    }

    @Override
    public List<MdClass> getAll(@PathVariable Integer page) {
        throw  new NotImplementedException();
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<MdClass> getAll() {
        try {
            List<MdClass> list = mdClassService.listAll();

            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MdClass getById(Long id) {
        throw  new NotImplementedException();
    }

    @Override
    public List<MdClass> getWithSearchCriteria(Integer searchCriteria) {
        throw  new NotImplementedException();
    }

    @Override
    public MdClass save(MdClass body) {
        throw  new NotImplementedException();
    }
}
