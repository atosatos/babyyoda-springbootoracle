package com.atos.MongoSpringRest.Controllers;

import com.atos.MongoSpringRest.Interface.ControllerInterface;
import com.atos.MongoSpringRest.Models.IspPickList.IspPickList;
import com.atos.MongoSpringRest.Services.IspPickList.IspPickListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/IspPickList")
public class IspPickListController implements ControllerInterface<IspPickList,IspPickListController> {

    private IspPickListService ispPickListService;

    @Autowired
    public void setIspPickListService(IspPickListService ispPickListService) {
        this.ispPickListService = ispPickListService;
    }


    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getById/{id}", produces = "application/json")
    public IspPickList getById(@PathVariable String id){
        try {
            return ispPickListService.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<IspPickList> getAll(Integer page) {
        throw new NotImplementedException();
    }
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/getAll", produces = "application/json")
    public List<IspPickList> getAll() {
        try {
            List<IspPickList> list = ispPickListService.listAll();
            if (list.isEmpty()) {
                return null;
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public IspPickList getById(Long id) {
        throw  new NotImplementedException();
    }

    @Override
    public List<IspPickList> getWithSearchCriteria(IspPickListController searchCriteria) {
        throw  new NotImplementedException();
    }

    @Override
    public IspPickList save(IspPickList body) {
        throw  new NotImplementedException();
    }

  /*  @CrossOrigin(origins = "*")
    @PostMapping(path = "/saveIspPickList", produces = "application/json")
    public IspPickList getById(@PathVariable String id){
        try {
            IspPickList element  = ispPickListService.getById(id);
            return element;
        } catch (Exception e) {
            return null;
        }
    }*/
}
