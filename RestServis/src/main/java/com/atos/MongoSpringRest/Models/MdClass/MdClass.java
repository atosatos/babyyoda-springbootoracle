package com.atos.MongoSpringRest.Models.MdClass;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class MdClass {

    @Id
    @Column(name="ID")
    private String id;
    @Column(name="CODE")
    private String code;
    @Column(name="NAME")
    private String name;
    @Column(name="CLASS_CATEGORY_ID")
    private int class_category_id;

    public MdClass() {
    }

    public MdClass(String id, String name, String code, int class_category_id) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.class_category_id = class_category_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getClass_category_id() {
        return class_category_id;
    }

    public void setClass_category_id(int class_category_id) {
        this.class_category_id = class_category_id;
    }
}
