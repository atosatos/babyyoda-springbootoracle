package com.atos.MongoSpringRest.Models.IspPickList;

import javax.persistence.*;


@Entity
public class IspPickList {

    @Id
    @Column(name="PICK_LIST_ID")
    private String pick_list_id;
    @Column(name="NAME")
    private String name;
    @Column(name="PICK_LIST_TYPE")
    private String pick_list_type;

    public IspPickList() {
    }

    public IspPickList(String id, String name, String code) {
        this.pick_list_id = id;
        this.name = name;
        this.pick_list_type = code;
    }

    public String getId() {
        return pick_list_id;
    }

    public void setId(String id) {
        this.pick_list_id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPick_list_type() {
        return pick_list_type;
    }

    public void setPick_list_type(String pick_list_type) {
        this.pick_list_type = pick_list_type;
    }
}
