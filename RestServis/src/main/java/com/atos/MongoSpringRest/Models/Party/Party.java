package com.atos.MongoSpringRest.Models.Party;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Data
@Entity
@Table(name = "Party")
public class Party implements java.io.Serializable{

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "MAIDEN_NAME")
    private String maidenName;
    @Column(name = "MOTHERS_NAME")
    private String mothersName;
    @Column(name = "FATHERS_NAME")
    private String fathersName;
    @Column(name = "MOTHERS_MAIDEN_NAME")
    private String mothersMaidenName;
    @Column(name = "PARENTS_LASTNAME")
    private String parentsLastname;
    @Column(name = "DATE_OF_BIRTH")
    private Date dateOfBirth;
    @Column(name = "CITY_OF_BIRTH")
    private String cityOfBirth;
    @Column(name = "CURRENT_ADDRESS")
    private String currentAddress;
    @Column(name = "CURRENT_ADDRESS_NO")
    private Integer currentAddressNo;
    @Column(name = "ID_NUMBER")
    private Long idNumber;
    @Column(name = "DRIVERS_LICENCE_NO")
    private Long driversLicenceNo;
    @Column(name = "ID_CARD_NO")
    private Long idCardNo;
    @Column(name = "PASSPORT_NUMBER")
    private Long passportNumber;
    @Column(name = "GENDER_ID")
    private String genderId;
    @Column(name = "DATE_OF_DEATH")
    private Date dateOfDeath;
    @Column(name = "CASEFILE_ID")
    private Long casefileId;
    @Column(name = "PERSON_ID")
    private Long personId;
    @Column(name = "PERSON_CATEGORY_ID")
    private Integer personCategoryId;
    @Column(name = "RELATED_PARTY_ID")
    private Integer relatedPartyId;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Nullable
    @Column(name = "MODIFIER_ID")
    private Integer modifierId;
    @Nullable
    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;
    @Column(name = "POZOVI")
    private Integer pozovi;
    @Column(name = "PARTY_ROLE_CODE")
    private String partyRoleCode;
    @Column(name = "PARTY_TYPE_CODE")
    private String partyTypeCode;
    @Column(name = "SHORT_NAME")
    private String shortName;
    @Column(name = "LEGAL_ENTITY_CODE")
    private String legalEntityCode;
    @Column(name = "PIB")
    private String pib;
    @Column(name = "FULL_NAME")
    private String fullName;


    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "casefile_id", referencedColumnName = "id", insertable = false,updatable = false)
    private Casefile casefileParty;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    public String getParentsLastname() {
        return parentsLastname;
    }

    public void setParentsLastname(String parentsLastname) {
        this.parentsLastname = parentsLastname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCityOfBirth() {
        return cityOfBirth;
    }

    public void setCityOfBirth(String cityOfBirth) {
        this.cityOfBirth = cityOfBirth;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public Integer getCurrentAddressNo() {
        return currentAddressNo;
    }

    public void setCurrentAddressNo(Integer currentAddressNo) {
        this.currentAddressNo = currentAddressNo;
    }

    public Long getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Long idNumber) {
        this.idNumber = idNumber;
    }

    public Long getDriversLicenceNo() {
        return driversLicenceNo;
    }

    public void setDriversLicenceNo(Long driversLicenceNo) {
        this.driversLicenceNo = driversLicenceNo;
    }

    public Long getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(Long idCardNo) {
        this.idCardNo = idCardNo;
    }

    public Long getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(Long passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getGenderId() {
        return genderId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public Date getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Long getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Long casefileId) {
        this.casefileId = casefileId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Integer getPersonCategoryId() {
        return personCategoryId;
    }

    public void setPersonCategoryId(Integer personCategoryId) {
        this.personCategoryId = personCategoryId;
    }

    public Integer getRelatedPartyId() {
        return relatedPartyId;
    }

    public void setRelatedPartyId(Integer relatedPartyId) {
        this.relatedPartyId = relatedPartyId;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Nullable
    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierId(@Nullable Integer modifierId) {
        this.modifierId = modifierId;
    }

    @Nullable
    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(@Nullable Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Integer getPozovi() {
        return pozovi;
    }

    public void setPozovi(Integer pozovi) {
        this.pozovi = pozovi;
    }

    public String getPartyRoleCode() {
        return partyRoleCode;
    }

    public void setPartyRoleCode(String partyRoleCode) {
        this.partyRoleCode = partyRoleCode;
    }

    public String getPartyTypeCode() {
        return partyTypeCode;
    }

    public void setPartyTypeCode(String partyTypeCode) {
        this.partyTypeCode = partyTypeCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLegalEntityCode() {
        return legalEntityCode;
    }

    public void setLegalEntityCode(String legalEntityCode) {
        this.legalEntityCode = legalEntityCode;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Casefile getCasefileParty() {
        return casefileParty;
    }

    public void setCasefileParty(Casefile casefileParty) {
        this.casefileParty = casefileParty;
    }
}
