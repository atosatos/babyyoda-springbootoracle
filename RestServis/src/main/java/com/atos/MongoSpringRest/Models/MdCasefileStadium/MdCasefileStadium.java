package com.atos.MongoSpringRest.Models.MdCasefileStadium;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class MdCasefileStadium {

    @Id
    @Column(name="ID")
    private String id;
    @Column(name="CODE")
    private String code;
    @Column(name="NAME")
    private String name;


    public MdCasefileStadium() {
    }

    public MdCasefileStadium(String id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
