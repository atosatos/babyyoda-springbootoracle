package com.atos.MongoSpringRest.Models.IspCaseSeqNum;

import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "ISP_CASE_SEQ_NUM")
public class IspCaseSeqNum{

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CASE_SN")
    private Integer caseSn;
    @Column(name = "CASE_ID")
    private Integer caseId;
    @Column(name = "CASE_CLASS_ID")
    private Integer caseClassId;
    @Column(name = "CASE_YEAR")
    private Integer caseYear;
    @Column(name = "CASE_JUDICIAL_INSTITUTION_CODE")
    private String caseJudicialInstitutionCode;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCaseSn() {
        return caseSn;
    }

    public void setCaseSn(Integer caseSn) {
        this.caseSn = caseSn;
    }

    public Integer getCaseId() {
        return caseId;
    }

    public void setCaseId(Integer caseId) {
        this.caseId = caseId;
    }

    public Integer getCaseClassId() {
        return caseClassId;
    }

    public void setCaseClassId(Integer caseClassId) {
        this.caseClassId = caseClassId;
    }

    public Integer getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(Integer caseYear) {
        this.caseYear = caseYear;
    }

    public String getCaseJudicialInstitutionCode() {
        return caseJudicialInstitutionCode;
    }

    public void setCaseJudicialInstitutionCode(String caseJudicialInstitutionCode) {
        this.caseJudicialInstitutionCode = caseJudicialInstitutionCode;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
