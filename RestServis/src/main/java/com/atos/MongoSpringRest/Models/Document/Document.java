package com.atos.MongoSpringRest.Models.Document;

import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Models.DocumentContent.DocumentContent;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "Document")
public class Document implements java.io.Serializable{

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "SENDER_TYPE_ID")
    private String senderTypeId;
    @Column(name = "DELIVERY_TYPE_ID")
    private Integer deliveryTypeId;
    @Column(name = "SENDER_NAME")
    private String senderName;
    @Column(name = "DOCUMENT_DATE")
    private Date documentDate;
    @Column(name = "INITIAL_ACT_DATE")
    private Date initialActDate;
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "EXTERNAL_CLASS_ID")
    private Integer externalClassId;
    @Column(name = "YEAR")
    private Integer year;
    @Column(name = "EXTERNAL_DOCUMENT_DATE")
    private Date extetnalDocumentDate;
    @Column(name = "EXTERNAL_NUMBER")
    private String externalNumber;
    @Column(name = "DOCUMENT_TYPE_ID")
    private int documentTypeId;
    @Column(name = "DOCUMENT_SUBTYPE_ID")
    private Integer documentSubtypeId ;
    @Column(name = "DOCUMENT_NUMBER")
    private String documentNumber;
    @Column(name = "PERSON_ID")
    private Integer personId;
    @Column(name = "DOCUMENT_OUTCOME_ID")
    private Integer documentOutComeId;
    @Column(name = "CASEFILE_ID")
    private int casefileId;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Column(name = "MODIFIER_ID")
    private Integer modifierId;
    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @Column(name = "INITIAL_ACT_DATE",insertable = false,updatable = false)
    private Date initialDateFrom;
    @Column(name = "INITIAL_ACT_DATE",insertable = false,updatable = false)
    private Date initialDateTo;

    @Column(name = "DOCUMENT_DATE",insertable = false,updatable = false)
    private Date documentDateFrom;
    @Column(name = "DOCUMENT_DATE",insertable = false,updatable = false)
    private Date documentDateTo;

    @JsonManagedReference
    @OneToMany(mappedBy = "document", cascade=CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<DocumentContent> documentContents;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSenderTypeId() {
        return senderTypeId;
    }

    public void setSenderTypeId(String senderTypeId) {
        this.senderTypeId = senderTypeId;
    }

    public Integer getDeliveryTypeId() {
        return deliveryTypeId;
    }

    public void setDeliveryTypeId(Integer deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Date getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }

    public Date getInitialActDate() {
        return initialActDate;
    }

    public void setInitialActDate(Date initialActDate) {
        this.initialActDate = initialActDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getExternalClassId() {
        return externalClassId;
    }

    public void setExternalClassId(Integer externalClassId) {
        this.externalClassId = externalClassId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Date getExtetnalDocumentDate() {
        return extetnalDocumentDate;
    }

    public void setExtetnalDocumentDate(Date extetnalDocumentDate) {
        this.extetnalDocumentDate = extetnalDocumentDate;
    }

    public String getExternalNumber() {
        return externalNumber;
    }

    public void setExternalNumber(String externalNumber) {
        this.externalNumber = externalNumber;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public Integer getDocumentSubtypeId() {
        return documentSubtypeId;
    }

    public void setDocumentSubtypeId(Integer documentSubtypeId) {
        this.documentSubtypeId = documentSubtypeId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getDocumentOutComeId() {
        return documentOutComeId;
    }

    public void setDocumentOutComeId(Integer documentOutComeId) {
        this.documentOutComeId = documentOutComeId;
    }

    public int getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(int casefileId) {
        this.casefileId = casefileId;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierId(Integer modifierId) {
        this.modifierId = modifierId;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getInitialDateFrom() {
        return initialDateFrom;
    }

    public void setInitialDateFrom(Date initialDateFrom) {
        this.initialDateFrom = initialDateFrom;
    }

    public Date getInitialDateTo() {
        return initialDateTo;
    }

    public void setInitialDateTo(Date initialDateTo) {
        this.initialDateTo = initialDateTo;
    }

    public Date getDocumentDateFrom() {
        return documentDateFrom;
    }

    public void setDocumentDateFrom(Date documentDateFrom) {
        this.documentDateFrom = documentDateFrom;
    }

    public Date getDocumentDateTo() {
        return documentDateTo;
    }

    public void setDocumentDateTo(Date documentDateTo) {
        this.documentDateTo = documentDateTo;
    }

    public Set<DocumentContent> getDocumentContents() {
        return documentContents;
    }

    public void setDocumentContents(Set<DocumentContent> documentContents) {
        this.documentContents = documentContents;
    }
}
