package com.atos.MongoSpringRest.Models.DisputeReason;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Data
@Entity
@Table(name = "DisputeReason")
public class DisputeReason {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CASEFILE_ID")
    private Integer casefileId;
    @Column(name = "DISPUTE_REASON_ID")
    private Integer disputeReasonId;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "CURRENCY_ID")
    private Integer currencyId;
    @Column(name = "SUBCATEGORY_ID")
    private Integer subcategoryId;
    @Column(name = "PRIMARY")
    private Integer primary;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Nullable
    @Column(name = "MODIFIER_ID")
    private Integer modifierId;
    @Nullable
    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;
    @Column(name = "PARTY_ID")
    private Integer partyId;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "casefile_id", referencedColumnName = "id", insertable = false,updatable = false)
    private Casefile casefileDisputeReason;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }

    public Integer getDisputeReasonId() {
        return disputeReasonId;
    }

    public void setDisputeReasonId(Integer disputeReasonId) {
        this.disputeReasonId = disputeReasonId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public Integer getPrimary() {
        return primary;
    }

    public void setPrimary(Integer primary) {
        this.primary = primary;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Nullable
    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierId(@Nullable Integer modifierId) {
        this.modifierId = modifierId;
    }

    @Nullable
    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(@Nullable Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public Casefile getCasefileDisputeReason() {
        return casefileDisputeReason;
    }

    public void setCasefileDisputeReason(Casefile casefileDisputeReason) {
        this.casefileDisputeReason = casefileDisputeReason;
    }
}
