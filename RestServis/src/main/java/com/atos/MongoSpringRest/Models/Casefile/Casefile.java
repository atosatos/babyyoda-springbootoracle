package com.atos.MongoSpringRest.Models.Casefile;

import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.context.annotation.Primary;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "Casefile")

public class Casefile implements java.io.Serializable{

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CLASS_ID")
    private Integer classId;
    @Column(name = "CASE_YEAR")
    private Integer caseYear;
    @Column(name = "CASE_SN")
    private Integer caseSn;
    @Column(name = "CASE_NUM")
    private String caseNum;
    @Column(name = "ORG_UNIT_ID")
    private int orgUnitId;
    @Column(name = "INITIAL_ACT_DATE")
    private Date initialDate;
    @Column(name = "JUDGE_ID")
    private int judgeId;
    @Column(name = "UNIQUE_CASE_SN")
    private int unigueCaseSn;
    @Column(name = "UNIQUE_CASE_NUM")
    private String unigue_case_num;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Column(name = "STADIUM_CODE")
    private int stadiumCode;

    @JsonManagedReference
    @OneToMany(mappedBy = "casefile", cascade=CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private Set<Link> link;

    @JsonManagedReference
    @OneToMany(mappedBy = "casefileParty", cascade=CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private Set<Party> party;

    @JsonManagedReference
    @OneToMany(mappedBy = "casefileDisputeReason", cascade=CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private Set<DisputeReason> disputeReasons;

    @JsonManagedReference

    @OneToMany(mappedBy = "casefileCriminalAct", cascade=CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private Set<CriminalAct> criminalActs;

    @Column(name = "INITIAL_ACT_DATE",insertable = false,updatable = false)
    private Date initialDateFrom;
    @Column(name = "INITIAL_ACT_DATE",insertable = false,updatable = false)
    private Date initialDateTo;

    public Date getInitialDateFrom() {
        return initialDateFrom;
    }

    public void setInitialDateFrom(Date initialDateFrom) {
        this.initialDateFrom = initialDateFrom;
    }

    public Date getInitialDateTo() {
        return initialDateTo;
    }

    public void setInitialDateTo(Date initialDateTo) {
        this.initialDateTo = initialDateTo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(Integer caseYear) {
        this.caseYear = caseYear;
    }

    public Integer getCaseSn() {
        return caseSn;
    }

    public void setCaseSn(Integer caseSn) {
        this.caseSn = caseSn;
    }

    public String getCaseNum() {
        return caseNum;
    }

    public void setCaseNum(String caseNum) {
        this.caseNum = caseNum;
    }

    public int getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(int orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public int getJudgeId() {
        return judgeId;
    }

    public void setJudgeId(int judgeId) {
        this.judgeId = judgeId;
    }

    public int getUnigueCaseSn() {
        return unigueCaseSn;
    }

    public void setUnigueCaseSn(int unigueCaseSn) {
        this.unigueCaseSn = unigueCaseSn;
    }

    public String getUnigue_case_num() {
        return unigue_case_num;
    }

    public void setUnigue_case_num(String unigue_case_num) {
        this.unigue_case_num = unigue_case_num;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getStadiumCode() {
        return stadiumCode;
    }

    public void setStadiumCode(int stadiumCode) {
        this.stadiumCode = stadiumCode;
    }

    public Set<Link> getLink() {
        return link;
    }

    public void setLink(Set<Link> link) {
        this.link = link;
    }

    public Set<Party> getParty() {
        return party;
    }

    public void setParty(Set<Party> party) {
        this.party = party;
    }

    public Set<DisputeReason> getDisputeReasons() {
        return disputeReasons;
    }

    public void setDisputeReasons(Set<DisputeReason> disputeReasons) {
        this.disputeReasons = disputeReasons;
    }

    public Set<CriminalAct> getCriminalActs() {
        return criminalActs;
    }

    public void setCriminalActs(Set<CriminalAct> criminalActs) {
        this.criminalActs = criminalActs;
    }
}
