package com.atos.MongoSpringRest.Models.CriminalActMultiValue;

import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CriminalActMultiValue1")
public class CriminalActMultiValue {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CRIMINAL_ACT_ID")
    private Integer criminalActId;
    @Column(name = "CASEFILE_ID")
    private Integer casefileId;
    @Column(name = "VALUE")
    private String value;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "criminal_act_id", referencedColumnName = "id", insertable = false,updatable = false)
    private CriminalAct criminalActMultiValue;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCriminalActId() {
        return criminalActId;
    }

    public void setCriminalActId(Integer criminalActId) {
        this.criminalActId = criminalActId;
    }

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CriminalAct getCriminalActMultiValue() {
        return criminalActMultiValue;
    }

    public void setCriminalActMultiValue(CriminalAct criminalActMultiValue) {
        this.criminalActMultiValue = criminalActMultiValue;
    }
}
