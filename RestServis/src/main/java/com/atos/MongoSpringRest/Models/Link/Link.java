package com.atos.MongoSpringRest.Models.Link;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "Link")
public class Link {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CASEFILE_ID")
    private Integer casefileId;
    @Column(name = "LINK_TYPE_ID")
    private Integer linkTypeId;
    @Column(name = "PERSON_ID")
    private Integer personId;
    @Column(name = "RELATED_CASEFILE_ID")
    private Integer relatedCasefileId;
    @Column(name = "CLASS_ID")
    private Integer classId;
    @Column(name = "RELATED_CASEFILE_NUMBER")
    private Integer relatedCasefileNumber;
    @Column(name = "RELATED_CASEFILE_YEAR")
    private Integer relatedCasefileYear;
    @Column(name = "RELATED_CASEFILE_DATE")
    private Date relatedCasefileDate;
    @Column(name = "RELATED_CASEFILE_DESCRIPTION")
    private String relatedCasefileDescription;
    @Column(name = "LINK_NUMBER")
    private String linkNumber;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Nullable
    @Column(name = "MODIFIER_ID")
    private Integer modifierId;
    @Nullable
    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "casefile_id", referencedColumnName = "id", insertable = false,updatable = false)
    private Casefile casefile;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }

    public Integer getLinkTypeId() {
        return linkTypeId;
    }

    public void setLinkTypeId(Integer linkTypeId) {
        this.linkTypeId = linkTypeId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getRelatedCasefileId() {
        return relatedCasefileId;
    }

    public void setRelatedCasefileId(Integer relatedCasefileId) {
        this.relatedCasefileId = relatedCasefileId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getRelatedCasefileNumber() {
        return relatedCasefileNumber;
    }

    public void setRelatedCasefileNumber(Integer relatedCasefileNumber) {
        this.relatedCasefileNumber = relatedCasefileNumber;
    }

    public Integer getRelatedCasefileYear() {
        return relatedCasefileYear;
    }

    public void setRelatedCasefileYear(Integer relatedCasefileYear) {
        this.relatedCasefileYear = relatedCasefileYear;
    }

    public Date getRelatedCasefileDate() {
        return relatedCasefileDate;
    }

    public void setRelatedCasefileDate(Date relatedCasefileDate) {
        this.relatedCasefileDate = relatedCasefileDate;
    }

    public String getRelatedCasefileDescription() {
        return relatedCasefileDescription;
    }

    public void setRelatedCasefileDescription(String relatedCasefileDescription) {
        this.relatedCasefileDescription = relatedCasefileDescription;
    }

    public String getLinkNumber() {
        return linkNumber;
    }

    public void setLinkNumber(String linkNumber) {
        this.linkNumber = linkNumber;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Nullable
    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierId(@Nullable Integer modifierId) {
        this.modifierId = modifierId;
    }

    @Nullable
    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(@Nullable Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Casefile getCasefile() {
        return casefile;
    }

    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }
}
