package com.atos.MongoSpringRest.Models.CriminalActInformation;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "CriminalActInformation")
public class CriminalActInformation {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CRIMINAL_ACT_ID")
    private Integer criminalActId;
    @Column(name = "CASEFILE_ID")
    private Integer casefileId;
    @Column(name = "LOV1")
    private String lov1;
    @Column(name = "LOV2")
    private String lov2;
    @Column(name = "LOV3")
    private String lov3;
    @Column(name = "LOV4")
    private String lov4;
    @Column(name = "LOV5")
    private String lov5;
    @Column(name = "TEXT1")
    private String text1;
    @Column(name = "TEXT2")
    private String text2;
    @Column(name = "TEXT3")
    private String text3;
    @Column(name = "TEXT4")
    private String text4;
    @Column(name = "TEXT5")
    private String text5;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "criminal_act_id", referencedColumnName = "id", insertable = false,updatable = false)
    private CriminalAct criminalActInformation;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCriminalActId() {
        return criminalActId;
    }

    public void setCriminalActId(Integer criminalActId) {
        this.criminalActId = criminalActId;
    }

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }

    public String getLov1() {
        return lov1;
    }

    public void setLov1(String lov1) {
        this.lov1 = lov1;
    }

    public String getLov2() {
        return lov2;
    }

    public void setLov2(String lov2) {
        this.lov2 = lov2;
    }

    public String getLov3() {
        return lov3;
    }

    public void setLov3(String lov3) {
        this.lov3 = lov3;
    }

    public String getLov4() {
        return lov4;
    }

    public void setLov4(String lov4) {
        this.lov4 = lov4;
    }

    public String getLov5() {
        return lov5;
    }

    public void setLov5(String lov5) {
        this.lov5 = lov5;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getText4() {
        return text4;
    }

    public void setText4(String text4) {
        this.text4 = text4;
    }

    public String getText5() {
        return text5;
    }

    public void setText5(String text5) {
        this.text5 = text5;
    }

    public CriminalAct getCriminalActInformation() {
        return criminalActInformation;
    }

    public void setCriminalActInformation(CriminalAct criminalActInformation) {
        this.criminalActInformation = criminalActInformation;
    }
}
