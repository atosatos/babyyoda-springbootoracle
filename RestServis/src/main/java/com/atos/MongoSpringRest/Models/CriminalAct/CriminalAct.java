package com.atos.MongoSpringRest.Models.CriminalAct;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.CriminalActInformation.CriminalActInformation;
import com.atos.MongoSpringRest.Models.CriminalActMultiValue.CriminalActMultiValue;
import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "CriminalAct")
public class CriminalAct {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "CASEFILE_ID")
    private Integer casefileId;
    @Column(name = "ARTICLE")
    private String article;
    @Column(name = "PARAGRAPH")
    private String paragraf;
    @Column(name = "ITEM")
    private String item;
    @Column(name = "ACT_TYPE_ID")
    private Integer actTypeId;
    @Column(name = "QUALIFICATION_CODE")
    private String qualificationCode;
    @Column(name = "EXTENDED_DURATION")
    private Integer extendedDuration;
    @Column(name = "DATE_OF_EXECUTION")
    private Date dateOfExecution;
    @Column(name = "DATE_FROM")
    private Date dateFrom;
    @Column(name = "DATE_TO")
    private Date dateTo;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "LAW_CODE")
    private String lawCode;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Nullable
    @Column(name = "MODIFIER_ID")
    private Integer modifierId;
    @Nullable
    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;
    @Column(name = "PARTY_ID")
    private Integer partyId;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "casefile_id", referencedColumnName = "id", insertable = false,updatable = false)
    private Casefile casefileCriminalAct;

    @JsonManagedReference
    @OneToMany(mappedBy = "criminalActInformation", cascade=CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<CriminalActInformation> criminalActInformations;

    @JsonManagedReference
    @OneToMany(mappedBy = "criminalActMultiValue", cascade=CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<CriminalActMultiValue> criminalActMultiValues;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getParagraf() {
        return paragraf;
    }

    public void setParagraf(String paragraf) {
        this.paragraf = paragraf;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getActTypeId() {
        return actTypeId;
    }

    public void setActTypeId(Integer actTypeId) {
        this.actTypeId = actTypeId;
    }

    public String getQualificationCode() {
        return qualificationCode;
    }

    public void setQualificationCode(String qualificationCode) {
        this.qualificationCode = qualificationCode;
    }

    public Integer getExtendedDuration() {
        return extendedDuration;
    }

    public void setExtendedDuration(Integer extendedDuration) {
        this.extendedDuration = extendedDuration;
    }

    public Date getDateOfExecution() {
        return dateOfExecution;
    }

    public void setDateOfExecution(Date dateOfExecution) {
        this.dateOfExecution = dateOfExecution;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getLawCode() {
        return lawCode;
    }

    public void setLawCode(String lawCode) {
        this.lawCode = lawCode;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Nullable
    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierId(@Nullable Integer modifierId) {
        this.modifierId = modifierId;
    }

    @Nullable
    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(@Nullable Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public Casefile getCasefileCriminalAct() {
        return casefileCriminalAct;
    }

    public void setCasefileCriminalAct(Casefile casefileCriminalAct) {
        this.casefileCriminalAct = casefileCriminalAct;
    }

    public Set<CriminalActInformation> getCriminalActInformations() {
        return criminalActInformations;
    }

    public void setCriminalActInformations(Set<CriminalActInformation> criminalActInformations) {
        this.criminalActInformations = criminalActInformations;
    }

    public Set<CriminalActMultiValue> getCriminalActMultiValues() {
        return criminalActMultiValues;
    }

    public void setCriminalActMultiValues(Set<CriminalActMultiValue> criminalActMultiValues) {
        this.criminalActMultiValues = criminalActMultiValues;
    }
}
