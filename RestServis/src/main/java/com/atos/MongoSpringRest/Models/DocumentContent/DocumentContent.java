package com.atos.MongoSpringRest.Models.DocumentContent;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Content.Content;
import com.atos.MongoSpringRest.Models.CriminalActInformation.CriminalActInformation;
import com.atos.MongoSpringRest.Models.CriminalActMultiValue.CriminalActMultiValue;
import com.atos.MongoSpringRest.Models.Document.Document;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "DocumentContent")
public class DocumentContent {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment",strategy = "increment")
    @Column(name = "ID")
    private long id;
    @Column(name = "DOCUMENT_ID")
    private Integer documentId;
    @Column(name = "CONTENT_ID")
    private Integer contentId;
    @Column(name = "SERIALNO")
    private Integer serialNo;
    @Column(name = "ACTIVE")
    private int active;
    @Column(name = "CREATOR_ID")
    private int creatorId;
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @Nullable
    @Column(name = "MODIFIER_ID")
    private Integer modifierId;
    @Nullable
    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "document_id", referencedColumnName = "id", insertable = false,updatable = false)
    private Document document;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "content_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Content content;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Nullable
    public Integer getModifierId() {
        return modifierId;
    }

    public void setModifierId(@Nullable Integer modifierId) {
        this.modifierId = modifierId;
    }

    @Nullable
    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(@Nullable Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }


    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }
}
