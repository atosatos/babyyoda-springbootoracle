package com.atos.MongoSpringRest.Specifications;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CaseFileSpecification implements Specification<Casefile> {

    private Casefile filter;

    public CaseFileSpecification(Casefile filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<Casefile> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getClassId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("classId"), filter.getClassId())));
        }
        if(filter.getCaseSn()!=0){
            p.getExpressions().add(
                    cb.and(cb.equal(root.get("caseSn"), filter.getCaseSn())));
        }

        if (filter.getCaseYear() != 0){
            p.getExpressions().add(cb.equal(root.get("caseYear"), filter.getCaseYear()));
        }

        if(filter.getInitialDateFrom()!=null){
            p.getExpressions().add(cb.greaterThanOrEqualTo(root.get("initialDateFrom"), filter.getInitialDateFrom()));
        }
        if(filter.getInitialDateTo()!=null){
            p.getExpressions().add(cb.lessThanOrEqualTo(root.get("initialDateTo"), filter.getInitialDateTo()));
        }
        return p;
    }
}
