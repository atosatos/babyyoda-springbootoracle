package com.atos.MongoSpringRest.Specifications.CriminalAct;


public class CriminalActSearchCriteria {
    private Integer casefileId;

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }
}
