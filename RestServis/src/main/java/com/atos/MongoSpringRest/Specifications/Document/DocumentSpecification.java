package com.atos.MongoSpringRest.Specifications.Document;

import com.atos.MongoSpringRest.Models.Document.Document;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class DocumentSpecification implements Specification<Document> {

    private Document filter;

    public DocumentSpecification(Document filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<Document> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getDeliveryTypeId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("deliveryTypeId"), filter.getDeliveryTypeId())));
        }
        if(filter.getSenderName()!=null){
            p.getExpressions().add(
                    cb.and(cb.equal(root.get("senderName"), filter.getSenderName())));
        }

        if (filter.getSenderTypeId() != null){
            p.getExpressions().add(cb.equal(root.get("senderTypeId"), filter.getSenderTypeId()));
        }
        if (filter.getDocumentTypeId() != 0){
            p.getExpressions().add(cb.equal(root.get("documentTypeId"), filter.getDocumentTypeId()));
        }
        if (filter.getDocumentNumber() != null){
            p.getExpressions().add(cb.equal(root.get("documentNumber"), filter.getDocumentNumber()));
        }

        if(filter.getInitialDateFrom()!=null){
            p.getExpressions().add(cb.greaterThanOrEqualTo(root.get("initialDateFrom"), filter.getInitialDateFrom()));
        }
        if(filter.getInitialDateTo()!=null){
            p.getExpressions().add(cb.lessThanOrEqualTo(root.get("initialDateTo"), filter.getInitialDateTo()));
        }

        if(filter.getDocumentDateFrom()!=null){
            p.getExpressions().add(cb.greaterThanOrEqualTo(root.get("documentDateFrom"), filter.getDocumentDateFrom()));
        }
        if(filter.getDocumentDateTo()!=null){
            p.getExpressions().add(cb.lessThanOrEqualTo(root.get("documentDateTo"), filter.getDocumentDateTo()));
        }
        return p;
    }
}
