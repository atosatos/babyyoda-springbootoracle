package com.atos.MongoSpringRest.Specifications.Party;

import com.atos.MongoSpringRest.Models.Party.Party;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PartySpecification implements Specification<Party> {

    private Party filter;

    public PartySpecification(Party filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<Party> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getCasefileId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("casefileId"), filter.getCasefileId())));
        }

        return p;
    }
}
