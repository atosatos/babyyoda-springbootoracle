package com.atos.MongoSpringRest.Specifications.Party;


public class PartySearchCriteria {

    private Long casefileId;

    public Long getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Long casefileId) {
        this.casefileId = casefileId;
    }
}
