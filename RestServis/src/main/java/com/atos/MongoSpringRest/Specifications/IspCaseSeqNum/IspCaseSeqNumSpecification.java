package com.atos.MongoSpringRest.Specifications.IspCaseSeqNum;

import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class IspCaseSeqNumSpecification implements Specification<IspCaseSeqNum> {

    private IspCaseSeqNum filter;

    public IspCaseSeqNumSpecification(IspCaseSeqNum filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<IspCaseSeqNum> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getCaseClassId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("caseClassId"), filter.getCaseClassId())));
        }
        if(filter.getCaseYear()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("caseYear"), filter.getCaseYear())));
        }
        if(filter.getCaseJudicialInstitutionCode()!=null){
            p.getExpressions().add(cb.and(cb.equal(root.get("caseJudicialInstitutionCode"), filter.getCaseJudicialInstitutionCode())));
        }
        cq.orderBy(cb.desc(root.get("caseSn")));

        return p;
    }
}
