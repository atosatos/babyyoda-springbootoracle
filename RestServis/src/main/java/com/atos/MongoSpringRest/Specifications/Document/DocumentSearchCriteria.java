package com.atos.MongoSpringRest.Specifications.Document;


import java.sql.Date;

public class DocumentSearchCriteria {

    private String senderTypeId;
    private Integer deliveryTypeId;
    private String senderName;
    private Date initialActDateFrom;
    private Date initialActDateTo;

    private Date documentDateFrom;
    private Date documentDateTo;

    private Integer documentTypeId;
    private String documentNumber;

    public String getSenderTypeId() {
        return senderTypeId;
    }

    public void setSenderTypeId(String senderTypeId) {
        this.senderTypeId = senderTypeId;
    }

    public Integer getDeliveryTypeId() {
        return deliveryTypeId;
    }

    public void setDeliveryTypeId(Integer deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Date getInitialActDateFrom() {
        return initialActDateFrom;
    }

    public void setInitialActDateFrom(Date initialActDateFrom) {
        this.initialActDateFrom = initialActDateFrom;
    }

    public Date getInitialActDateTo() {
        return initialActDateTo;
    }

    public void setInitialActDateTo(Date initialActDateTo) {
        this.initialActDateTo = initialActDateTo;
    }

    public Date getDocumentDateFrom() {
        return documentDateFrom;
    }

    public void setDocumentDateFrom(Date documentDateFrom) {
        this.documentDateFrom = documentDateFrom;
    }

    public Date getDocumentDateTo() {
        return documentDateTo;
    }

    public void setDocumentDateTo(Date documentDateTo) {
        this.documentDateTo = documentDateTo;
    }

    public Integer getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(Integer documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
