package com.atos.MongoSpringRest.Specifications.Link;


import java.sql.Date;

public class LinkSearchCriteria {

    private Integer casefileId;

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }
}
