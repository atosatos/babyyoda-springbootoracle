package com.atos.MongoSpringRest.Specifications.DisputeReason;

import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class DisputeReasonSpecification implements Specification<DisputeReason> {

    private DisputeReason filter;

    public DisputeReasonSpecification(DisputeReason filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<DisputeReason> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getCasefileId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("casefileId"), filter.getCasefileId())));
        }

        return p;
    }
}
