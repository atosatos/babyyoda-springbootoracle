package com.atos.MongoSpringRest.Specifications.DisputeReason;


public class DisputeReasonSearchCriteria {

    private Integer casefileId;

    public Integer getCasefileId() {
        return casefileId;
    }

    public void setCasefileId(Integer casefileId) {
        this.casefileId = casefileId;
    }
}
