package com.atos.MongoSpringRest.Specifications.Link;

import com.atos.MongoSpringRest.Models.Link.Link;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class LinkSpecification implements Specification<Link> {

    private Link filter;

    public LinkSpecification(Link filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<Link> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getCasefileId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("casefileId"), filter.getCasefileId())));
        }

        return p;
    }
}
