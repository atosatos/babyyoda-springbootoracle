package com.atos.MongoSpringRest.Specifications.CriminalAct;

import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.atos.MongoSpringRest.Models.Party.Party;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CriminalActSpecification implements Specification<CriminalAct> {

    private CriminalAct filter;

    public CriminalActSpecification(CriminalAct filter) {
        super();
        this.filter = filter;
    }

    public Predicate toPredicate(Root<CriminalAct> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder cb) {

        Predicate p = cb.conjunction();
        if(filter.getCasefileId()!=0){
            p.getExpressions().add(cb.and(cb.equal(root.get("casefileId"), filter.getCasefileId())));
        }

        return p;
    }
}
