package com.atos.MongoSpringRest.Specifications;


import java.sql.Date;

public class CasefileSearchCriteria {

    private int classId;
    private int caseSn;
    private int caseYear;
    private Date initialDateFrom;
    private Date initialDateTo;

    public Date getInitialDateTo() {
        return initialDateTo;
    }

    public void setInitialDateTo(Date initialDateTo) {
        this.initialDateTo = initialDateTo;
    }

    public Date getInitialDateFrom() {
        return initialDateFrom;
    }

    public void setInitialDateFrom(Date initialDateFrom) {
        this.initialDateFrom = initialDateFrom;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getCaseSn() {
        return caseSn;
    }

    public void setCaseSn(int caseSn) {
        this.caseSn = caseSn;
    }

    public int getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(int caseYear) {
        this.caseYear = caseYear;
    }
}
