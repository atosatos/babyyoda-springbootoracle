package com.atos.MongoSpringRest.Specifications.IspCaseSeqNum;


public class IspCaseSeqNumSearchCriteria {

    private Integer classId;
    private Integer caseYear;
    private String judicialCode;
    private Integer caseSn;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getCaseYear() {
        return caseYear;
    }

    public void setCaseYear(Integer caseYear) {
        this.caseYear = caseYear;
    }

    public String getJudicialCode() {
        return judicialCode;
    }

    public void setJudicialCode(String judicialCode) {
        this.judicialCode = judicialCode;
    }

    public Integer getCaseSn() {
        return caseSn;
    }

    public void setCaseSn(Integer caseSn) {
        this.caseSn = caseSn;
    }
}
