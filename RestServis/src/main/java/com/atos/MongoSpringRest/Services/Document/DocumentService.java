package com.atos.MongoSpringRest.Services.Document;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Document.Document;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Document.DocumentSearchCriteria;

import java.util.List;

public interface DocumentService {

    List<Document> listAll(int page);

    Document getById(Long id);

    Document saveOrUpdate(Document product);

    void delete(Long id);

    public List<Document> findDocumentBySearchCriteria(DocumentSearchCriteria searchCriteria);


}
