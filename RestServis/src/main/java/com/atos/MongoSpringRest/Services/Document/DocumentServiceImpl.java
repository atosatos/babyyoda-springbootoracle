package com.atos.MongoSpringRest.Services.Document;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Document.Document;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Repositories.Document.DocumentRepository;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Specifications.CaseFileSpecification;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Document.DocumentSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentServiceImpl implements DocumentService {
    private DocumentRepository documentRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public DocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }


    @Override
    public List<Document> listAll(int page) {
        Pageable pageable = PageRequest.of(page-1, 10);

        List<Document> listDocuments = new ArrayList<>();
        documentRepository.findAll(pageable).forEach(listDocuments::add);
        return listDocuments;
    }

    @Override
    public Document getById(Long id) {
        Document result =null;
        Optional<Document> document = documentRepository.findById(id);
        if(document.isPresent()){
            result =  document.get();
        }
        return result;
    }

    @Override
    public Document saveOrUpdate(Document document) {
        documentRepository.save(document);
        return document;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<Document> findDocumentBySearchCriteria(DocumentSearchCriteria searchCriteria) {
        return null;
    }
}
