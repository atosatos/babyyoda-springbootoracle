package com.atos.MongoSpringRest.Services.MdCasefileStadium;
import com.atos.MongoSpringRest.Models.MdCasefileStadium.MdCasefileStadium;

import java.util.List;

public interface MdCasefileStadiumService {

    List<MdCasefileStadium> listAll();


}
