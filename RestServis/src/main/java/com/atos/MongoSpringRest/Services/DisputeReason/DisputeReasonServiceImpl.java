package com.atos.MongoSpringRest.Services.DisputeReason;

import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.atos.MongoSpringRest.Repositories.DisputeReason.DisputeReasonRepository;
import com.atos.MongoSpringRest.Repositories.Party.PartyRepository;
import com.atos.MongoSpringRest.Services.Party.PartyService;
import com.atos.MongoSpringRest.Specifications.DisputeReason.DisputeReasonSearchCriteria;
import com.atos.MongoSpringRest.Specifications.DisputeReason.DisputeReasonSpecification;
import com.atos.MongoSpringRest.Specifications.Party.PartySearchCriteria;
import com.atos.MongoSpringRest.Specifications.Party.PartySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DisputeReasonServiceImpl implements DisputeReasonService {
    private DisputeReasonRepository disputeReasonRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public DisputeReasonServiceImpl(DisputeReasonRepository disputeReasonRepository) {
        this.disputeReasonRepository = disputeReasonRepository;
    }

    /**
     * get all Dispute Reasons from database
     * @return
     */
    @Override
    public List<DisputeReason> listAll() {
        List<DisputeReason> listDisputeReasons = new ArrayList<>();
        disputeReasonRepository.findAll().forEach(listDisputeReasons::add);
        return listDisputeReasons;
    }

    /**
     * get Dispute Reason element from database by Id
     * @param id
     * @return
     */
    @Override
    public DisputeReason getById(Long id) {
        DisputeReason result = null;
        Optional<DisputeReason> disputeReason = disputeReasonRepository.findById(id);
        if(disputeReason.isPresent()){
            result = disputeReason.get();
        }
        return result;
    }

    @Override
    public List<DisputeReason> getByCasefileId(Long casefileId) {
        List<DisputeReason> listDisputeReasons = new ArrayList<>();

        return listDisputeReasons;
    }

    /**
     * Save or Update Dispute Reason element
     * @param disputeReason
     * @return
     */
    @Override
    public DisputeReason saveOrUpdate(DisputeReason disputeReason) {
        disputeReasonRepository.save(disputeReason);
        return disputeReason;
    }

    /**
     * Set Active to 0 on Casefile element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }

    @Override
    public List<DisputeReason> findDisputeReasonBySearchCriteria(DisputeReasonSearchCriteria searchCriteria) {
        DisputeReason disputeReason = new DisputeReason();
        disputeReason.setCasefileId(searchCriteria.getCasefileId());

        Specification<DisputeReason> spec = new DisputeReasonSpecification(disputeReason);

        return disputeReasonRepository.findAll(spec);
    }




}
