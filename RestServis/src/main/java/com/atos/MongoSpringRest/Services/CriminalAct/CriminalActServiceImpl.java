package com.atos.MongoSpringRest.Services.CriminalAct;

import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.atos.MongoSpringRest.Repositories.CriminalAct.CriminalActRepository;
import com.atos.MongoSpringRest.Specifications.CriminalAct.CriminalActSearchCriteria;
import com.atos.MongoSpringRest.Specifications.CriminalAct.CriminalActSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CriminalActServiceImpl implements CriminalActService {
    private CriminalActRepository criminalActRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public CriminalActServiceImpl(CriminalActRepository criminalActRepository) {
        this.criminalActRepository = criminalActRepository;
    }

    /**
     * get all Criminal Acts from database
     * @return
     */
    @Override
    public List<CriminalAct> listAll() {
        List<CriminalAct> listCriminalActs = new ArrayList<>();
        criminalActRepository.findAll().forEach(listCriminalActs::add);
        return listCriminalActs;
    }

    /**
     * get Criminal Act element from database by Id
     * @param id
     * @return
     */
    @Override
    public CriminalAct getById(Long id) {
        CriminalAct result = null;
        Optional<CriminalAct> criminalAct = criminalActRepository.findById(id);
        if(criminalAct.isPresent()){
            result = criminalAct.get();
        }
        return result;
    }

    @Override
    public List<CriminalAct> getByCasefileId(Long casefileId) {
        List<CriminalAct> listCriminalActs = new ArrayList<>();

        return listCriminalActs;
    }

    /**
     * Save or Update Criminal Act element
     * @param criminalAct
     * @return
     */
    @Override
    public CriminalAct saveOrUpdate(CriminalAct criminalAct) {
        criminalActRepository.save(criminalAct);
        return criminalAct;
    }

    /**
     * Set Active to 0 on Criminal Act element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }

    @Override
    public List<CriminalAct> findCriminalActBySearchCriteria(CriminalActSearchCriteria searchCriteria) {
        CriminalAct criminalAct = new CriminalAct();
        criminalAct.setCasefileId(searchCriteria.getCasefileId());

        Specification<CriminalAct> spec = new CriminalActSpecification(criminalAct);

        return criminalActRepository.findAll(spec);
    }



}
