package com.atos.MongoSpringRest.Services.Casefile;

import com.atos.MongoSpringRest.Specifications.CaseFileSpecification;
import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CaseFileServiceImpl implements  CasefileService{
    private CasefileRepository casefileRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public CaseFileServiceImpl(CasefileRepository casefileRepository) {
        this.casefileRepository = casefileRepository;
    }

    /**
     * get all Casefile from database
     * @return
     */
    @Override
    public List<Casefile> listAll(int page) {
        Pageable pageable = PageRequest.of(page-1, 10);

        List<Casefile> listCasefiles = new ArrayList<>();
        casefileRepository.findAll(pageable).forEach(listCasefiles::add);
        return listCasefiles;
    }

    /**
     * get Casefile element from database by Id
     * @param id
     * @return
     */
    @Override
    public Casefile getById(Long id) {
        Casefile result = null;
        Optional<Casefile> casefile = casefileRepository.findById(id);
        if(casefile.isPresent()){
            result =  casefile.get();
        }
        return result;
    }

    /**
     * Save or Update casefile element
     * @param casefile
     * @return
     */
    @Override
    public Casefile saveOrUpdate(Casefile casefile) {
        casefileRepository.save(casefile);
        return casefile;
    }

    /**
     * Set Active to 0 on Casefile element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }
    /**
     * return all casefile with criteria
     * @param searchCriteria
     * @return
     */

    @Override
    public List<Casefile> findCaseFileBySearchCriteria(CasefileSearchCriteria searchCriteria) {
        Casefile casefile = new Casefile();
        casefile.setCaseYear(searchCriteria.getCaseYear());
        casefile.setCaseSn(searchCriteria.getCaseSn());
        casefile.setClassId(searchCriteria.getClassId());
        casefile.setInitialDateFrom(searchCriteria.getInitialDateFrom());
        casefile.setInitialDateTo(searchCriteria.getInitialDateTo());

        Specification<Casefile> spec = new CaseFileSpecification(casefile);

        return casefileRepository.findAll(spec);
    }


}
