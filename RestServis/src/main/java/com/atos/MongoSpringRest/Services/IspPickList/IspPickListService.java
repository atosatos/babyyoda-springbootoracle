package com.atos.MongoSpringRest.Services.IspPickList;

import com.atos.MongoSpringRest.Models.IspPickList.IspPickList;

import java.util.List;

public interface IspPickListService {

    List<IspPickList> listAll();

    IspPickList getById(String id);

    IspPickList saveOrUpdate(IspPickList product);

    void delete(String id);

}
