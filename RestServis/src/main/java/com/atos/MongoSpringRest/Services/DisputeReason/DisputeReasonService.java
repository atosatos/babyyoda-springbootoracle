package com.atos.MongoSpringRest.Services.DisputeReason;

import com.atos.MongoSpringRest.Models.DisputeReason.DisputeReason;
import com.atos.MongoSpringRest.Specifications.DisputeReason.DisputeReasonSearchCriteria;

import java.util.List;

public interface DisputeReasonService {

    List<DisputeReason> listAll();

    DisputeReason getById(Long id);

    List<DisputeReason> getByCasefileId(Long casefileId);

    DisputeReason saveOrUpdate(DisputeReason product);

    void delete(Long id);

    public List<DisputeReason> findDisputeReasonBySearchCriteria(DisputeReasonSearchCriteria searchCriteria);


}
