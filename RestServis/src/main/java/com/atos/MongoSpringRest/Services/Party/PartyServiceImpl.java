package com.atos.MongoSpringRest.Services.Party;

import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Models.Party.Party;
import com.atos.MongoSpringRest.Repositories.Link.LinkRepository;
import com.atos.MongoSpringRest.Repositories.Party.PartyRepository;
import com.atos.MongoSpringRest.Services.Link.LinkService;
import com.atos.MongoSpringRest.Specifications.Link.LinkSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Link.LinkSpecification;
import com.atos.MongoSpringRest.Specifications.Party.PartySearchCriteria;
import com.atos.MongoSpringRest.Specifications.Party.PartySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PartyServiceImpl implements PartyService {
    private PartyRepository partyRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public PartyServiceImpl(PartyRepository partyRepository) {
        this.partyRepository = partyRepository;
    }

    /**
     * get all Parties from database
     * @return
     */
    @Override
    public List<Party> listAll() {
        List<Party> listParties = new ArrayList<>();
        partyRepository.findAll().forEach(listParties::add);
        return listParties;
    }

    /**
     * get Party element from database by Id
     * @param id
     * @return
     */
    @Override
    public Party getById(Long id) {
        Party result = null;
        Optional<Party> party = partyRepository.findById(id);
        if(party.isPresent()){
            result = party.get();
        }
        return result;
    }

    @Override
    public List<Party> getByCasefileId(Long casefileId) {
        List<Party> listParties = new ArrayList<>();

        return listParties;
    }

    /**
     * Save or Update Party element
     * @param party
     * @return
     */
    @Override
    public Party saveOrUpdate(Party party) {
        partyRepository.save(party);
        return party;
    }

    /**
     * Set Active to 0 on Casefile element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }

    @Override
    public List<Party> findPartyBySearchCriteria(PartySearchCriteria searchCriteria) {

        Party party = new Party();
        party.setCasefileId(searchCriteria.getCasefileId());

        Specification<Party> spec = new PartySpecification(party);

        return partyRepository.findAll(spec);
    }


}
