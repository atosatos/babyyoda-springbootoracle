package com.atos.MongoSpringRest.Services.Party;

import com.atos.MongoSpringRest.Models.Party.Party;
import com.atos.MongoSpringRest.Specifications.Link.LinkSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Party.PartySearchCriteria;

import java.util.List;

public interface PartyService {

    List<Party> listAll();

    Party getById(Long id);

    List<Party> getByCasefileId(Long casefileId);

    Party saveOrUpdate(Party product);

    void delete(Long id);

    public List<Party> findPartyBySearchCriteria(PartySearchCriteria searchCriteria);


}
