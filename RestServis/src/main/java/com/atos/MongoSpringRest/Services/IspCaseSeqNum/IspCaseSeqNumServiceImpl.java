package com.atos.MongoSpringRest.Services.IspCaseSeqNum;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Repositories.IspCaseSeqNum.IspCaseSeqNumRepository;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Specifications.CaseFileSpecification;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.IspCaseSeqNum.IspCaseSeqNumSearchCriteria;
import com.atos.MongoSpringRest.Specifications.IspCaseSeqNum.IspCaseSeqNumSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class IspCaseSeqNumServiceImpl implements IspCaseSeqNumService {
    private IspCaseSeqNumRepository ispCaseSeqNumRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public IspCaseSeqNumServiceImpl(IspCaseSeqNumRepository ispCaseSeqNumRepository) {
        this.ispCaseSeqNumRepository = ispCaseSeqNumRepository;
    }


    @Override
    public IspCaseSeqNum getById(Long id) {
        IspCaseSeqNum result = null;
        Optional<IspCaseSeqNum> casefile = ispCaseSeqNumRepository.findById(id);
        if(casefile.isPresent()){
            result =  casefile.get();
        }
        return result;
    }

    @Override
    public IspCaseSeqNum saveOrUpdate(IspCaseSeqNum ispCaseSeqNum) {
        ispCaseSeqNumRepository.save(ispCaseSeqNum);
        return ispCaseSeqNum;
    }

    @Override
    public long findMaxIdBySearchCriteria(IspCaseSeqNumSearchCriteria searchCriteria) {
        IspCaseSeqNum result =null;
        List<IspCaseSeqNum> list =null;
        IspCaseSeqNum ispCaseSeqNum = new IspCaseSeqNum();
        ispCaseSeqNum.setCaseYear(searchCriteria.getCaseYear());
        ispCaseSeqNum.setCaseClassId(searchCriteria.getClassId());
        ispCaseSeqNum.setCaseJudicialInstitutionCode(searchCriteria.getJudicialCode());

        Specification<IspCaseSeqNum> spec = new IspCaseSeqNumSpecification(ispCaseSeqNum);
        try{
            list =ispCaseSeqNumRepository.findAll(spec);

        }catch (Exception ex){

        }
         if(list.size()!=0){
             result = list.get(0);
             long a =list.get(0).getCaseSn() +1;
         }
         if(result==null)
             return  1;

        return result.getCaseSn() +1;
    }
}
