package com.atos.MongoSpringRest.Services.Content;

import com.atos.MongoSpringRest.Models.Content.Content;
import com.atos.MongoSpringRest.Models.DocumentContent.DocumentContent;
import com.atos.MongoSpringRest.Repositories.Content.ContentRepository;
import com.atos.MongoSpringRest.Repositories.DocumentContent.DocumentContentRepository;
import com.atos.MongoSpringRest.Services.DocumentContent.DocumentContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContentServiceImpl implements ContentService {
    private ContentRepository contentRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public ContentServiceImpl(ContentRepository contentRepository) {
        this.contentRepository = contentRepository;
    }

    /**
     * get all Content from database
     * @return
     */
    @Override
    public List<Content> listAll() {
        List<Content> listContents = new ArrayList<>();
        contentRepository.findAll().forEach(listContents::add);
        return listContents;
    }

    /**
     * get Content element from database by Id
     * @param id
     * @return
     */
    @Override
    public Content getById(Long id) {
        Content result = null;
        Optional<Content> content = contentRepository.findById(id);
        if(content.isPresent()){
            result =  content.get();
        }
        return result;
    }

    /**
     * Save or Update Content element
     * @param content
     * @return
     */
    @Override
    public Content saveOrUpdate(Content content) {
        contentRepository.save(content);
        return content;
    }

    /**
     * Set Active to 0 on Content element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }


}
