package com.atos.MongoSpringRest.Services.IspPickList;

import com.atos.MongoSpringRest.Models.IspPickList.IspPickList;
import com.atos.MongoSpringRest.Repositories.IspPickList.IspPickListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class IspPickListServiceImpl implements  IspPickListService{
    private IspPickListRepository ispPickListRepository;


    @Autowired
    public IspPickListServiceImpl(IspPickListRepository ispPickListRepository) {
        this.ispPickListRepository = ispPickListRepository;
    }

    /**
     * all elements from IspPickList table
     * @return List<IspPickList>
     */
    @Override
    public List<IspPickList> listAll() {
        List<IspPickList> listIspPickLists = new ArrayList<>();
        ispPickListRepository.findAll().forEach(listIspPickLists::add);
        return listIspPickLists;
    }

    /***
     * element from IspPickList table by id
     * @param id
     * @return IspPickList
     */
    @Override
    public IspPickList getById(String id) {
        IspPickList result = null;
        Optional<IspPickList> pickList = ispPickListRepository.findById(id);
        if(pickList.isPresent()){
            result =  pickList.get();
        }
        return result;
    }

    /**
     *  save or update IspPickList element
     * @param picklist
     * @return IspPickList
     */
    @Override
    public IspPickList saveOrUpdate(IspPickList picklist) {
        ispPickListRepository.save(picklist);
        return picklist;
    }

    @Override
    public void delete(String id) {

    }
}
