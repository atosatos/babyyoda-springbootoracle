package com.atos.MongoSpringRest.Services.IspCaseSeqNum;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.IspCaseSeqNum.IspCaseSeqNum;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.IspCaseSeqNum.IspCaseSeqNumSearchCriteria;

import java.util.List;

public interface IspCaseSeqNumService {

    IspCaseSeqNum getById(Long id);
    IspCaseSeqNum saveOrUpdate(IspCaseSeqNum product);


    public long findMaxIdBySearchCriteria(IspCaseSeqNumSearchCriteria searchCriteria);


}
