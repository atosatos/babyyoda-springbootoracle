package com.atos.MongoSpringRest.Services.Casefile;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;

import java.util.List;

public interface CasefileService {

    List<Casefile> listAll(int page);

    Casefile getById(Long id);

    Casefile saveOrUpdate(Casefile product);

    void delete(Long id);

    public List<Casefile> findCaseFileBySearchCriteria(CasefileSearchCriteria searchCriteria);


}
