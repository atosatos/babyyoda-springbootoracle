package com.atos.MongoSpringRest.Services.MdClass;

import com.atos.MongoSpringRest.Models.MdClass.MdClass;
import com.atos.MongoSpringRest.Repositories.MdClass.MdClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MdClassServiceImpl implements MdClassService {
    private MdClassRepository mdClassRepository;


    @Autowired
    public MdClassServiceImpl(MdClassRepository mdClassRepository) {
        this.mdClassRepository = mdClassRepository;
    }

    /**
     * all elements from MdClass table
     * @return List<MdClass>
     */
    @Override
    public List<MdClass> listAll() {
        List<MdClass> listMdClasses = new ArrayList<>();
        mdClassRepository.findAll().forEach(listMdClasses::add);
        return listMdClasses;
    }

}
