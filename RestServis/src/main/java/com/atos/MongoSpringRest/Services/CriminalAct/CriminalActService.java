package com.atos.MongoSpringRest.Services.CriminalAct;

import com.atos.MongoSpringRest.Models.CriminalAct.CriminalAct;
import com.atos.MongoSpringRest.Specifications.CriminalAct.CriminalActSearchCriteria;

import java.util.List;

public interface CriminalActService {

    List<CriminalAct> listAll();

    CriminalAct getById(Long id);

    List<CriminalAct> getByCasefileId(Long casefileId);

    CriminalAct saveOrUpdate(CriminalAct product);

    void delete(Long id);

    public List<CriminalAct> findCriminalActBySearchCriteria(CriminalActSearchCriteria searchCriteria);


}
