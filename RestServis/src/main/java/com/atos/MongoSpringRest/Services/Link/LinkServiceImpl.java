package com.atos.MongoSpringRest.Services.Link;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Repositories.Link.LinkRepository;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Specifications.CaseFileSpecification;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Link.LinkSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Link.LinkSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LinkServiceImpl implements LinkService {
    private LinkRepository linkRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public LinkServiceImpl(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    /**
     * get all Links from database
     * @return
     */
    @Override
    public List<Link> listAll() {
        List<Link> listLinks = new ArrayList<>();
        linkRepository.findAll().forEach(listLinks::add);
        return listLinks;
    }

    /**
     * get Link element from database by Id
     * @param id
     * @return
     */
    @Override
    public Link getById(Long id) {
        Link result = null;
        Optional<Link> link = linkRepository.findById(id);
        if(link.isPresent()){
            result = link.get();
        }
        return result;
    }

    @Override
    public List<Link> getByCasefileId(Long casefileId) {
        List<Link> listLinks = new ArrayList<>();

        return listLinks;
    }

    /**
     * Save or Update Link element
     * @param link
     * @return
     */
    @Override
    public Link saveOrUpdate(Link link) {
        linkRepository.save(link);
        return link;
    }

    /**
     * Set Active to 0 on Casefile element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }

    @Override
    public List<Link> findLinkBySearchCriteria(LinkSearchCriteria searchCriteria) {

        Link link = new Link();
        link.setCasefileId(searchCriteria.getCasefileId());

        Specification<Link> spec = new LinkSpecification(link);

        return linkRepository.findAll(spec);
    }


}
