package com.atos.MongoSpringRest.Services.DocumentContent;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.DocumentContent.DocumentContent;
import com.atos.MongoSpringRest.Repositories.Casefile.CasefileRepository;
import com.atos.MongoSpringRest.Repositories.DocumentContent.DocumentContentRepository;
import com.atos.MongoSpringRest.Services.Casefile.CasefileService;
import com.atos.MongoSpringRest.Specifications.CaseFileSpecification;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentContentServiceImpl implements DocumentContentService {
    private DocumentContentRepository documentContentRepository;
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public DocumentContentServiceImpl(DocumentContentRepository documentContentRepository) {
        this.documentContentRepository = documentContentRepository;
    }

    /**
     * get all DocumentContent from database
     * @return
     */
    @Override
    public List<DocumentContent> listAll() {
        List<DocumentContent> listDocumentContents = new ArrayList<>();
        documentContentRepository.findAll().forEach(listDocumentContents::add);
        return listDocumentContents;
    }

    /**
     * get DocumentContent element from database by Id
     * @param id
     * @return
     */
    @Override
    public DocumentContent getById(Long id) {
        DocumentContent result = null;
        Optional<DocumentContent> documentContent = documentContentRepository.findById(id);
        if(documentContent.isPresent()){
            result =  documentContent.get();
        }
        return result;
    }

    /**
     * Save or Update DocumentContent element
     * @param documentContent
     * @return
     */
    @Override
    public DocumentContent saveOrUpdate(DocumentContent documentContent) {
        documentContentRepository.save(documentContent);
        return documentContent;
    }

    /**
     * Set Active to 0 on DocumentContent element
     * @param id
     */
    @Override
    public void delete(Long id) {

    }


}
