package com.atos.MongoSpringRest.Services.Content;

import com.atos.MongoSpringRest.Models.Content.Content;

import java.util.List;

public interface ContentService {

    List<Content> listAll();

    Content getById(Long id);

    Content saveOrUpdate(Content product);

    void delete(Long id);

}
