package com.atos.MongoSpringRest.Services.MdCasefileStadium;

import com.atos.MongoSpringRest.Models.MdCasefileStadium.MdCasefileStadium;
import com.atos.MongoSpringRest.Repositories.MdCasefileStadium.MdCasefileStadiumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MdCasefileStadiumServiceImpl implements MdCasefileStadiumService {
    private MdCasefileStadiumRepository mdCasefileStadiumRepository;


    @Autowired
    public MdCasefileStadiumServiceImpl(MdCasefileStadiumRepository mdCasefileStadiumRepository) {
        this.mdCasefileStadiumRepository = mdCasefileStadiumRepository;
    }

    /**
     * all elements from MdClass table
     * @return List<MdClass>
     */
    @Override
    public List<MdCasefileStadium> listAll() {
        List<MdCasefileStadium> listMdCasefileStadium = new ArrayList<>();
        mdCasefileStadiumRepository.findAll().forEach(listMdCasefileStadium::add);
        return listMdCasefileStadium;
    }

}
