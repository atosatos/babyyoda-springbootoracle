package com.atos.MongoSpringRest.Services.DocumentContent;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.DocumentContent.DocumentContent;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;

import java.util.List;

public interface DocumentContentService {

    List<DocumentContent> listAll();

    DocumentContent getById(Long id);

    DocumentContent saveOrUpdate(DocumentContent product);

    void delete(Long id);

}
