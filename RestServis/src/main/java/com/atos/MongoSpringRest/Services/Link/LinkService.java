package com.atos.MongoSpringRest.Services.Link;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Models.Link.Link;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import com.atos.MongoSpringRest.Specifications.Link.LinkSearchCriteria;

import java.util.List;

public interface LinkService {

    List<Link> listAll();

    Link getById(Long id);

    List<Link> getByCasefileId(Long casefileId);

    Link saveOrUpdate(Link product);

    void delete(Long id);

    public List<Link> findLinkBySearchCriteria(LinkSearchCriteria searchCriteria);


}
