package com.atos.MongoSpringRest.Interface;

import com.atos.MongoSpringRest.Models.Casefile.Casefile;
import com.atos.MongoSpringRest.Specifications.CasefileSearchCriteria;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface ControllerInterface<T,S> {

    public List<T> getAll(@PathVariable Integer page);

    public T getById(@PathVariable Long id);

    public List<T> getWithSearchCriteria(S searchCriteria);

    public T save(@RequestBody T body);

}
